// validate email as required field and format

(function($) {
  /**
   * trim a string
   * @param {string to trim} s
   */
  function trim(s) {
    return s.replace(/^\s+|\s+$/, "");
  }

  /**
   * check whether is an array
   * @param {array} arr
   */
  function isArray(arr) {
    return Object.prototype.toString.call(arr) === "[object Array]";
  }

  /**
   * valid rules
   */
  var validateRules = {
    /**validate an email */
    email: function(input) {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(trim(input));
    },
    /**validate a required field */
    required: function(input) {
      if (!input) return false;
      return trim(input).length > 0;
    },
    /**validate min length of the input */
    minLength: function(input, min) {
      if (!input) return false;
      return trim(input).length >= min;
    },
    /**validate min length of checcbox */
    minOptions: function(input, min) {
      if (!isArray(input)) return false;
      return input.length >= min;
    },
    /** custom validate rule */
    tigerType: function(input) {
      if ($("#tiger").is(":checked")) {
        return trim(input).length > 0;
      }
      return true;
    }
  };

  /**logic to handle the validation */
  var validateForm = function(validates) {
    /**remove the error class every time trigger the submit button */
    $("p").removeClass("error");

    var errors = [];

    /**validate every define rule */
    validates.forEach(function(validate) {
      var el = $(validate.el);
      var rules = validate.rules;

      /**get value of the checkbox group */
      if (validate.type === "checkbox") {
        var value = [];
        $(validate.checkedEl).each(function() {
          value.push($(this).val());
        });
      } else {
        var value = el.val();
      }
      for (var key in rules) {
        if (!validateRules[key]) return false;
        if (!validateRules[key](value, rules[key])) {
          /**store all the errors, we can handle error message later */
          errors.push(el);
          el.parent("p").addClass("error");
        }
      }
    });

    return errors;
  };

  $(function() {
    var validates = [
      {
        el: "#email",
        rules: { required: true, email: true }
      },
      {
        el: "#password",
        rules: { required: true, minLength: 8 }
      },
      {
        el: "#colour",
        rules: { required: true }
      },
      {
        el: "#bear",
        type: "checkbox",
        checkedEl: "input[name='animal']:checked",
        rules: { minOptions: 2 }
      },
      {
        el: "#tiger_type",
        rules: { tigerType: true }
      }
    ];

    $("#form").submit(function(e) {
      e.preventDefault();
      var errors = validateForm(validates);
      if (errors.length > 0) {
        return false;
      } else {
        alert("form is valid and ready to post");
      }
    });
  });
})($ || window.jQuery);
