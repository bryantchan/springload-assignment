Demo url: https://springload-assignment.now.sh

Hi, my name is Ronghuan Chen. I spent about 1.5 hours on this assignment. Most of the time was not coding but considering what technique to use. Finally, I decided to use pure JavaScript and jQuery for dom handling. 

I wrote a small but reusable validate function. Since it's written by vanilla JavaScript, we don't need to worry about too much with the browser support. We can also enhance the application by adding more validate rules or showing the error messages in the future.

I didn't write the e2e test due to the time limitation, but it's not difficult to do it using e2e frameworks like cypress.
